import React from 'react'

class PresentationForm extends React.Component {
    constructor(props){
        super(props)
        this.state={
            name: '',
            title: '',
            email: '',
            company: '',
            synopsis: '',
            conferences: [],
        };
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleCompany = this.handleCompany.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleSynopsis = this.handleSynopsis.bind(this);
        this.handleConference = this.handleConference.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleName(event) {
        const value = event.target.value;
        this.setState({name:value});
    }

    handleEmail(event) {
        const value = event.target.value;
        this.setState({email:value})
    }
    handleCompany(event) {
        const value = event.target.value;
        this.setState({company:value})
    }
    handleTitle(event) {
        const value = event.target.value;
        this.setState({title:value});
    }
    handleSynopsis(event) {
        const value = event.target.value;
        this.setState({synopsis:value});
    }
    handleConference(event) {
        const value = event.target.value;
        this.setState({conference:value});
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.name;
        data.presenter_email = data.email;
        data.company_name = data.company;
        delete data.name
        delete data.email
        delete data.conferences;
        delete data.company
        console.log(data)

        const conferenceUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            const newPresentation = await response.json();
            console.log(newPresentation)
            const cleared = {
                company: '',
                name: '',
                email: '',
                synopsis: '',
                title: '',
                conferences: [],
            }
            this.setState(cleared);
        }
    }



    async componentDidMount(){
        const url = "http://localhost:8000/api/conferences/"

        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            this.setState({conferences: data.conferences})
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form id="create-presentation-form" onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input placeholder="Name" onChange={this.handleName} value={this.state.name} required type="text" id="name" name="presenter_name" className="form-control"/>
                                <label htmlFor="name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="presenter_email" onChange={this.handleEmail} value={this.state.email} required type="email" id="presenter_email" className="presenter_email" className="form-control"/>
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="company_name" onChange={this.handleCompany} value={this.state.company} type="text" name="company_name" id="company_name" className="form-control"/>
                                <label htmlFor="company_name">Company Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="title" onChange={this.handleTitle}required value={this.state.title}  type="text" name="title" id="title" className="form-control"/>
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea className="form-control" onChange={this.handleSynopsis} value={this.state.synopsis} placeholder="synopsis" required type="text" name="synopsis" id="synopsis"></textarea>
                                <label htmlFor="synopsis" className="form-label">Synopsis</label>
                            </div>
                            <div className="mb-3">
                                <select required id="conference" onChange={this.handleConference} name="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {this.state.conferences.map(conference => {
                                    return (
                                        <option key={conference.id} value={conference.id}>
                                            {conference.name}
                                        </option>
                                    );
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
          </div>
        );
    }
}

export default PresentationForm