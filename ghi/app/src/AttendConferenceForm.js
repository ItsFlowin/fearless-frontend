import React from 'react'

class AttendConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            conferences: [],
            hasSignedUp: false,
        }
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleConference = this.handleConference.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        delete data.hasSignedUp;
        console.log(data)

        const url = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            const success = await response.json();
            console.log(success)

            const cleared = {
                name: '',
                email:'',
                conference: '',
                hasSignedUp: true,
            };
            this.setState(cleared);
        }

    }

    handleEmail(event) {
        const value = event.target.value;
        this.setState({email: value})

    }

    handleName(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleConference(event){
        const value = event.target.value;
        this.setState({conference: value});
    }


    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url)
        if(response.ok){
            const data = await response.json();
            this.setState({conferences: data.conferences})
        }
    }


    render(){
        let spinnerClasses = "d-flex justify-content-center mb-3";
        let dropdownClasses = "form-select d-none";
        if (this.state.conferences.length > 0) {
            spinnerClasses = "d-flex justify-content-center mb-3 d-none"
            dropdownClasses = "form-select"
        }
        let successClasses = "alert alert-success d-none mb-0"
        let formClasses='';
        if (this.state.hasSignedUp) {
            successClasses = "alert alert-success mb-0";
            formClasses = 'd-none';
        }

        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form className={formClasses} id="create-attendee-form" onSubmit={this.handleSubmit}>
                                <h1 className="card-title">It's Conference Time!</h1>
                                <p className="mb-3">
                                    Please choose which conference
                                    you'd like to attend.
                                </p>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select name="conference" onChange={this.handleConference} value={this.state.conference} id="conference" className={dropdownClasses} required>
                                    <option value="">Choose a conference</option>
                                    {this.state.conferences.map(conference=>{
                                        return (
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        )
                                    })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Now, tell us about yourself.
                                </p>
                                <div className="row">
                                    <div className="col">
                                    <div className="form-floating mb-3">
                                        <input required placeholder="Your full name" onChange={this.handleName} value={this.state.name} type="text" id="name" name="name" className="form-control"/>
                                        <label htmlFor="name">Your full name</label>
                                    </div>
                                    </div>
                                    <div className="col">
                                    <div className="form-floating mb-3">
                                        <input required placeholder="Your email address" onChange={this.handleEmail} value={this.state.email} type="email" id="email" name="email" className="form-control"/>
                                        <label htmlFor="email">Your email address</label>
                                    </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">I'm going!</button>
                                </form>
                                <div className="alert alert-success d-none mb-0" id="success-message">
                                Congratulations! You're all signed up!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        )
    }
}

export default AttendConferenceForm