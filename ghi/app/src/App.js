import Nav from './Nav';
import AttendeesList from './Attendeeslist';
import LocationForm from './LocationForm';
import ConferenceForm from './new-conference';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes,Route } from "react-router-dom"
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/">
            <Route path="attendees" element = {<AttendeesList attendees={props.attendees} />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm/>} />
          </Route>
          <Route path ="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm/>} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
