import React from 'react'

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentation: '',
            maxAttendee: '',
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.handleDescChange = this.handleDescChange.bind(this);
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
        this.handleMaxAttendeeChange = this.handleMaxAttendeeChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_attendees = data.maxAttendee;
        data.max_presentations = data.maxPresentation;
        delete data.maxAttendee;
        delete data.maxPresentation;
        delete data.locations
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentation: '',
                maxAttendee: '',
                location: [],
            };
            this.setState(cleared)
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url)

        if(response.ok){
            const data = await response.json();
            this.setState({locations: data.locations})
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    }
    handleStartChange(event) {
        const value = event.target.value;
        this.setState({starts: value});
    }
    handleEndChange(event) {
        const value = event.target.value;
        this.setState({ends: value});
    }
    handleDescChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    }
    handleMaxPresentationChange(event) {
        const value = event.target.value;
        this.setState({maxPresentation: value});
    }
    handleMaxAttendeeChange(event) {
        const value = event.target.value;
        this.setState({maxAttendee: value});
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStartChange} value={this.state.starts} placeholder="Start Date" required type="date" name="starts" id="start_date" className="form-control"/>
                                <label htmlFor="start">Start Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEndChange} value={this.state.ends} placeholder="End Date" required type="date" name="ends" id="end_date" className="form-control"/>
                                <label htmlFor="end">End Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea onChange={this.handleDescChange} value={this.state.description} className="form-control" placeholder="Descriptions" required type="text" name="description" id="description"></textarea>
                                <label htmlFor="description" className="form-label">Description</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxPresentationChange} value={this.state.maxPresentation} placeholder="Maximum Presentations" required type="number" name="max_presentations" id="max_presentation" className="form-control"/>
                                <label htmlFor="max_presentations">Maximum Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxAttendeeChange} value={this.state.maxAttendee} placeholder="Maximum Attendees" required type="number" name="max_attendees" id="max_attendee" className="form-control"/>
                                <label htmlFor="max_attendees">Maximum Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {this.state.locations.map(location => {
                                    return (
                                        <option key = {location.href} value = {location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default ConferenceForm